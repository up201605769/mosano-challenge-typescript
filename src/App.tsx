import React from 'react';
import './App.css';

function App() {
  return (
    <div className="App">
      <body>
        <div className="row">
          <div className="column left">
            <form>
              <div>
                <label htmlFor="name">Name:</label>
                <input id="name" name="name" type="text" />
              </div>

              <div>
                <label htmlFor="lastname">Surname:</label>
                <input id="lastname" name="surname" type="text" />
              </div>

              <div>
                <label htmlFor="countries">Countries:</label>
                <select id="countries" name="countries" />
              </div>

              <div>
                <label htmlFor="birthday">Birthday:</label>
                <input id="birthday" name="birthday" type="date" />
              </div>

              <button type="submit" value="Submit">Save</button>
            </form>
          </div>

          <div className="column right">
            <table>
                <thead>
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Country</th>
                        <th scope="col">Birthday</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
          </div>
        </div>
      </body>
    </div>
  );
}

export default App;
